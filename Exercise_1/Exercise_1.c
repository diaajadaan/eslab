/*
* Exercise_1.c
*
* Created: 26.08.2014 11:47:49
*  Author: group11
*/

#include "defines.h"
#include <avr/io.h>
#include <avr/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "uart.h"
#include "joy.h"
#include <stdio.h>
#include "oled.h"
#include "UI.h"
#include <avr/pgmspace.h>

char RX_flag=0;
char TX_flag=0;
byte adc;

const char* str[5] ={"LEFT","RIGHT","UP","DOWN","CENTER"};
enum { BUTTONJOY=1,LTOUCH,RTOUCH};

//Structure describes current menu and submenu state
struct Menu_State{
	char menuNo;//1,2,3,4
	char subMenuNo;//1,2,3
} MN;

ISR(USART0_TXC_vect)
{
	TX_flag=1;
}

ISR(USART0_RXC_vect)
{
	char data=UDR0;
	data=data;		//make Atmel happy
	RX_flag=1;
}

ISR(INT0_vect)
{
	adc_done();
}

void int_init()
{
	//Enable INT0
	set_bit(MCUCR,ISC01);
	clear_bit(MCUCR,ISC00);
	set_bit(GICR,INT0);
}

void lifeSignal()
{
	set_bit(PORTB,LED);
	_delay_ms(LIFE_PERIOD/2);
	clear_bit(PORTB,LED);
	_delay_ms(LIFE_PERIOD/2);
}

void SRAM_init(void){
	DDRC = 0xFF;
	PORTC = 0x00;
	SFIOR = (1<<XMM2);
	set_bit(MCUCR, 7);
}

void SRAM_test(void)
{
	//SRAM test provided by exercise
	ext_ram=(char *) 0x1800;
	volatile unsigned int i, werrors, rerrors;
	werrors = 0;
	rerrors = 0;
	unsigned char testvalue;
	printf("Starting SRAM test...\r\n");
	rerrors=rerrors;
	for (i = 0; i < 0x800; i++) {
		testvalue = ~(i % 256);
		ext_ram[i] = testvalue;
		if (ext_ram[i] != testvalue) {
			//printf("SRAM error (write phase): ext_ram[%d] = %02X (should be %02X)\r\n", i, ext_ram[i], testvalue);
			werrors++;
		}
	}
	
	for (i = 0; i < 0x800; i++) {
		testvalue = ~(i % 256);
		if (ext_ram[i] != testvalue) {
			//printf("SRAM error (read phase): ext_ram[%d] = %02X (should be%02X)\r\n", i, ext_ram[i], testvalue);
			rerrors++;
		}
	}
	printf("SRAM test completed with %d errors in write phase and %d errors in read	phase\r\n", werrors, rerrors);
}

void uart_test()
{
	//if(TX_flag)
	//{
	//TX_flag=0;
	//set_bit(PORTB,0);
	//_delay_ms(200);
	//clear_bit(PORTB,0);
	//_delay_ms(1000);
	//}
	
	if(RX_flag)
	{
		RX_flag=0;
		set_bit(PORTB,1);
		_delay_ms(200);
		clear_bit(PORTB,1);
	}
	//uart_transmit(c++);
	_delay_ms(10);
}

void print_joy_status()
{
	joy_slide_t pos_s=joy_getSliders();
	printf("Left slider=%u%%",pos_s.Left);
	printf("\tRight slider=%u%%",pos_s.Right);
	printf("\t");

	printf("Left=%u",joy_button(LTOUCH));
	printf("\tRight=%u",joy_button(RTOUCH));
	printf("\tJoy=%u",joy_button(BUTTONJOY));
	printf("\t");

	joy_pos_t pos_a=joy_getPosition();
	printf("X=%d%%",pos_a.X);
	printf("\tY=%d%%",pos_a.Y);
	printf("\tPos=%s",str[joy_getDirection()]);
	printf("\n");
}

void init()
{
	//Ports initialization
	DDRB=0x07;

	sei();

	//Peripherals initialization
	uart_init ( MYUBRR );
	int_init();
	SRAM_init();
	SRAM_test();
	joy_init();
	oled_init();
	oled_goto_line(0);
	//oled_test();
	
}

typedef void (*FuncPtr)(void);
typedef struct menu_item{
	struct menu_item* parent;
	struct menu_item* child;
	struct menu_item* upSibling;
	struct menu_item* downSibling;
	char level;
	char order;
	char* label;
	FuncPtr func;
} menu_item_t;

void put_menu(menu_item_t* x)
{
	char init_order=x->order;
	do{
		oled_goto_line(x->order);
		oled_putc(' ');
		oled_puts_const(x->label);
		x=x->downSibling;
	}while(x->order!=init_order);
}

void menu_item_init(menu_item_t* item,menu_item_t* upSibling, menu_item_t* downSibling, menu_item_t* parent,menu_item_t* child,int level, int order, char* label, FuncPtr function)
{
	item->upSibling=upSibling;
	item->downSibling=downSibling;
	item->parent=parent;
	item->child=child;
	item->label=label;
	item->level=level;
	item->order=order;
	item->func=function;
}

void menu_create(menu_item_t** menu_array,char size,char level,char* labels[], FuncPtr* functions)
{
	for(char i=0;i<size;i++)
	{
		menu_item_init(menu_array[i],menu_array[(i-1)<0?size-1:i-1],menu_array[(i+1)>size-1?0:i+1],NULL,NULL,level,i,labels[i],functions[i]);
	}
}

void menu_link(menu_item_t* item, menu_item_t** menu_array, char array_size)
{
	item->child=menu_array[0];
	for(char i=0;i<array_size;i++)
		menu_array[i]->parent=item;
}

void func00(void);
void func01(void);
void func10(void);
void func11(void);
void func20(void);
void func21(void);

void func01(void){}
void func10(void){}
void func11(void){}
void func20(void){}
void func21(void){oled_puts(" This is 2.1");}
void func00(void)
{
	oled_puts(" TEST");
}

FuncPtr FuncTable0[]={func00, func01};			//functions for submenu0
FuncPtr FuncTable1[]={func10, func11};			//functions for submenu1
FuncPtr FuncTable2[]={func20, func21};			//functions for submenu2

int main(void)
{
	init();
	FuncPtr FPtr=NULL;

	/*
	char test[]= "Hello world";
	oled_goto_line(0);
	oled_puts(test);
	oled_goto_line(0);
	current_line=0;
	//oled_goto_home();
	current_menu=0;
	current_submenu=0;
	*/
	int menu_items_num[4]={3,2,2,2};
	menu_item_t current_item,x0,x1,x2;
	menu_item_t x00,x01;
	menu_item_t x10,x11;
	menu_item_t x20,x21;
	menu_item_t** menu_arr;
	menu_item_t* menu_array[3]={&x0,&x1,&x2};
	menu_item_t* menu_array0[2]={&x00,&x01};
	menu_item_t* menu_array1[2]={&x10,&x11};
	menu_item_t* menu_array2[2]={&x20,&x21};
	
	char size;
	
	size=menu_items_num[0];
	menu_arr=menu_array;
	menu_create(menu_array,menu_items_num[0],0,MENU,NULL);
	menu_create(menu_array0,menu_items_num[1],1,SUBMENU0,FuncTable0);
	menu_create(menu_array1,menu_items_num[2],1,SUBMENU1,FuncTable1);
	menu_create(menu_array2,menu_items_num[3],1,SUBMENU2,FuncTable2);

	menu_link(&x0,menu_array0,menu_items_num[1]);
	menu_link(&x1,menu_array1,menu_items_num[2]);
	menu_link(&x2,menu_array2,menu_items_num[3]);
	//size=menu_items_num[3];
	//menu_arr=menu_array2;
	//for(char i=0;i<size;i++)
		//menu_item_init(menu_arr[i],menu_arr[(i-1)<0?size-1:i-1],menu_arr[(i+1)>size-1?0:i+1],NULL,NULL,1,i,SUBMENU2[i]);
	/*
	menu_item_init(&x0,&x2,&x1,NULL,&x00,0,0,"first");
	
	x00.upSibling=&x01;
	x00.downSibling=&x01;
	//x00.child=&x00;
	x00.parent=&x0;
	x00.label="0-0";
	x00.level=1;
	x00.order=0;
	*/
	current_item=x0;
	oled_clear_display();
	put_menu(&current_item);
	oled_goto_home();
	oled_putc('-');
	/*
	for(char i=0;i<4;i++)
	{
	oled_goto_line(i);
	oled_putc(' ');
	oled_puts_const(MENU[i]);
	}*/
	while(1)
	{
		if(joy_getDirection()!=CENTER)
		{
			switch (joy_getDirection())
			{
				case DOWN:
				current_item=*current_item.downSibling;
				break;
				case UP:
				current_item=*current_item.upSibling;
				break;
				case RIGHT:
				if(current_item.child!=NULL)
				current_item=*current_item.child;
				break;
				case LEFT:
				if(current_item.parent!=NULL)
				current_item=*current_item.parent;
				break;
			}
			oled_clear_display();
			put_menu(&current_item);
			oled_clear_indents();
			oled_goto_line(current_item.order);
			oled_putc('-');	
			FPtr=current_item.func;
			while(joy_getDirection()!=CENTER);
		}
		if(joy_button(BUTTONJOY))
		{		
			FPtr();
			while(joy_button(JOYBUTTON));
		}
		_delay_ms(100);
		lifeSignal();
		//print_joy_status();
	}
}