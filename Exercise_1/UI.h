/*
 * UI.h
 *
 * Created: 23.09.2014 12:59:15
 *  Author: thaddauh
 */ 


#ifndef UI_H_
#define UI_H_

#include <avr/pgmspace.h>

typedef void (*FuncPtr)(void);
//function pointer
FuncPtr FPtr;

//Menu Strings in flash
//menu 1
const char MN0[] PROGMEM="<<Zeroth>>\0";
//menu 2
const char MN1[] PROGMEM="<<First>>\0";
//menu 3
const char MN3[] PROGMEM="<<Second>>\0";
//menu 4
const char MN4[] PROGMEM="<<Third>>\0";

//SubMenu Strings in flash
//menu 0 submenus
const char MN00[] PROGMEM="0.0\0";
const char MN01[] PROGMEM="0.1\0";
const char MN02[] PROGMEM="0.2\0";
//Submenus of menu 1
const char MN10[] PROGMEM="1.0\0";
const char MN11[] PROGMEM="1.1\0";
//Submenus of menu 2
const char MN20[] PROGMEM="2.0\0";
const char MN21[] PROGMEM="2.1\0";
//submenus of menu 3
const char MN30[] PROGMEM="3.0\0";
const char MN31[] PROGMEM="3.1\0";

const char *MENU[] ={
	MN0,	//menu 1 string
	MN1,	//menu 2 string
	MN3,	//menu 3 string
	MN4	//menu 4 string
};

const char *SUBMENU0[] ={
	MN00, MN01, MN02	//submenus of menu 0
};

const char *SUBMENU1[] ={
	MN10, MN11			//submenus of menu 1
};

const char *SUBMENU2[] ={
	MN20, MN21			//submenus of menu 2
};

const char *SUBMENU3[] ={
	MN30, MN31			//submenus of menu 3
};

//FPtr=(void*)pgm_read_word(&FuncPtrTable[MFIndex(MN.menuNo, MN.subMenuNo)]);



#endif /* UI_H_ */