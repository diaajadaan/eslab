/*
 * joy.c
 *
 * Created: 09.09.2014 14:55:54
 *  Author: diaaj
 */ 

#include <avr/io.h>
#include "defines.h"
#include "joy.h"

signed char offset_x, offset_y;

void joy_init()
{
	adc_init();
	joy_calibrate();
	set_bit(JOYBUTTONPORT,JOYBUTTON);
}

void joy_calibrate()
{
	//offset_x=127-(char)adc_read(2);
	byte x,y ;
	x=adc_read(2);
	y=adc_read(1);
	offset_x=128-(signed char)x;
	offset_y=128-(signed char)y;	
}

joy_pos_t joy_getPosition()
{
	joy_pos_t pos;
	pos.X = (adc_read(2)-(127 - offset_x))*100/128 ;	//position in % seen from the calibrated zero point -100%<---0--->+100%
	pos.Y = (adc_read(1)-(127 - offset_y))*100/128;
	return pos;
}

joy_slide_t joy_getSliders()
{
	joy_slide_t pos;
	pos.Right = adc_read(3)*100/255 ;	//position in % seen from the calibrated zero point -100%<---0--->+100%
	pos.Left = adc_read(4)*100/255;
	return pos;
}

//param: 1-joystick 2-left touch 3-right touch
bool joy_button(int button)
{
	switch (button)
	{
		case 1:
			return !(test_bit(JOYBUTTONPIN,JOYBUTTON)>0);
		break;
		case 2:
			return test_bit(TOUCHLPIN,TOUCHL)>0;
		break;
		default:
			return test_bit(TOUCHRPIN,TOUCHR)>0;
		break;
	}
}

joy_dir_t joy_getDirection()
{
	joy_pos_t pos=joy_getPosition();
	signed char x, y;
	char abs_x,abs_y;
	x=pos.X;
	y=pos.Y;
	abs_x=x>0?x:-1*x;
	abs_y=x>0?y:-1*y;
	if((x<3 && x>-3) && (y<3 && y>-3))
		return CENTER;
	else if(x>0 && abs_x>abs_y)
		return RIGHT;
	else if(x<0 && abs_x>abs_y)
		return LEFT;
	else if(y>0 && abs_x<abs_y)
		return UP;
	else 
		return DOWN;
		
	//if(x>y && y>0 && x>0)
		//return joy_dir_t.RIGHT;
	//else if(x>y && y>0 && x>0)
		//return joy_dir_t.RIGHT;	
	//else if(x>y && y<0)
		//return joy_dir_t.RIGHT;
		/*
	if(x==0 && y==0)
	return CENTER;			
	if(x>0)
		{
			if(y>0)
				if(x>y)
					return RIGHT;
				else 
					return UP;
			else
				if(x<-y)
					return RIGHT;
				else 
					return DOWN;
		}
	else 
	{
		if(y>0)	
			if(x<y)
				return LEFT;
			else
				return UP;
		else
			if(x<y)
				return LEFT;
			else
				return DOWN;
	}
	*/
	
}
