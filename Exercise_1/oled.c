/*
* oled.c
*
* Created: 16.09.2014 15:33:20
*  Author: diaaj
*/

#include <avr/io.h>
#include "defines.h"
#include <avr/delay.h>
#include "oled.h"
#include "font.h"
//#include <avr/pgmspace.h>

const char col_width=5;
char oled_buffer[20];
//volatile unsigned char *oled_dual_buffer; 
/*				128
			|--------|
 8*8 bit 	|        |
			|--------|
*/
volatile unsigned char oled_pos_row, oled_pos_column;

/*
void oled_dualbuf_goto_pos(char row, char column, char mod){
	// mod specifies if the position determines full characters or individual eight bit pages
	// |input|  operation
	// |0| 		standard characters used as specified in CHARACTER_WIDTH
	// |1| 		column defines individual bits 
	if (mod == 0){
	oled_pos_row = row;
	oled_pos_column= column*128/CHARACTER_WIDTH;
	}
	else if (mod == 1){
	oled_pos_row = row;
	oled_pos_column= column;
	}
}
void oled_dualbuf_put_byte(unsigned char b){

	oled_dual_buffer[oled_pos_row][oled_pos_column]= b;// write value into byte
	
	//advance in the buffer by one position:
	if (oled_pos_column < 127)
	oled_dualbuf_goto_pos(oled_pos_row,oled_pos_column+1, 1);
	else 
	{
		if (oled_pos_row <7)
			oled_dualbuf_goto_pos(oled_pos_row+1,0,1);
		else
			oled_dualbuf_goto_pos(0,0,1);
	}
}
void oled_flush()
{
	oled_goto_home();
	for(int i=0;i<8;i++)
	for(int j=0;j<127;j++)
	oled_put_byte(oled_dual_buffer[i][j]);
}

*/

void oled_init()
{
	ext_ram = (char *) 0x1000 ; // Start address for the OLED command
	ext_ram[0] =0xae; // display off
	ext_ram[0] =0xa1; //segment remap
	ext_ram[0] =0xda; //common pads hardware: alternative
	ext_ram[0] =0x12;
	ext_ram[0] =0xc8; //common output scan direction:com63~com0
	ext_ram[0] =0xa8; //multiplex ration mode:63
	ext_ram[0] =0x3f;
	ext_ram[0] =0xd5; //display divide ratio/osc. freq. mode
	ext_ram[0] =0x80;
	ext_ram[0] =0x81; //contrast control
	ext_ram[0] =0x50;
	ext_ram[0] =0xd9; //set pre-charge period
	ext_ram[0] =0x21;
	
	ext_ram[0] =0x20; //Set Memory Addressing Mode to Horizontal
	ext_ram[0] =0x00;
	ext_ram[0] =0x40; //Set display start from COM0
	
	ext_ram[0] =0xdb; //VCOM deselect level mode
	ext_ram[0] =0x30;
	ext_ram[0] =0xad; //master configuration
	ext_ram[0] =0x00;
	ext_ram[0] =0xa4; //out follows RAM content
	ext_ram[0] =0xa6; //set normal display
	ext_ram[0] =0xaf; // display on
	oled_clear_display();
	
	//fdevopen(&oled_putc_printf, NULL);
	
	//dualbuffer init
	oled_pos_row = 0;
	oled_pos_column = 0;	
	//oled_dual_buffer = 0x1800;
	}

void oled_test()
{
	ext_ram = (char *) 0x1200 ; // Start address for the OLED data
	for(char j=1;j<3;j++)
	for(char i=0;i<4;i++)
	{	ext_ram[0] =font[j][i];
		_delay_ms(10);
	}
}

void oled_send_cmd(char* buffer, char buffer_size)
{
	char i;
	ext_ram = (char *) 0x1000 ; // Start address for the OLED command
	for(i=0; i<buffer_size; i++)
	ext_ram[0] = buffer[i];
}

void oled_goto_home()
{
	oled_goto_pos(0,0);
	current_line=0;
}

void oled_goto_line(char line)
{
	oled_goto_pos(line,0);
	//current_line=line;
}

void oled_goto_pos(char row,char column)
{
	oled_buffer[0] =0x21; //Set column start and end address
	oled_buffer[1] =0x7f & column;
	oled_buffer[2] =0x7f;
	oled_buffer[3] =0x22; //Set page start and end address
	oled_buffer[4] =0x07 & row;
	oled_buffer[5] =0x07;
	oled_send_cmd(oled_buffer,6);
	//current_line=row;
}

void oled_put_byte(unsigned char b)
{
	ext_ram = (char *) 0x1200 ; // Start address for the oled
	ext_ram[0] =  b;
}

int oled_putc_printf(char  string, FILE *stream)
{
	unsigned char* char_ptr;
	char i;
	//if (string == '\n')
	//oled_putc('\r', stream);
	char_ptr = font[(string - 32)];
	for(i = 0; i < CHARACTER_WIDTH; i++)
	//oled_put_byte(pgm_read_word(char_ptr[i]));
	oled_put_byte(char_ptr[i]);
}

void oled_putc(unsigned char c)
{
	unsigned char* char_ptr;
	char i;
	//if (string == '\n')
	//oled_putc('\r');
	char_ptr = font[(c - 32)];
	for(i = 0; i < CHARACTER_WIDTH; i++)
	oled_put_byte(pgm_read_byte(&char_ptr[i]));
	//oled_dualbuf_put_byte(pgm_read_byte(&char_ptr[i]));
	//oled_put_byte(char_ptr[i]);
}

void oled_puts(char * string)
{
	while(*string != 0){
		oled_putc(*string);
		string++;		
	}
}

void oled_puts_const(const char * string)
{
	while(pgm_read_byte(string) != 0){
		oled_putc(pgm_read_byte(string));
		string++;
	}
}

void oled_clear_line(byte line)
{
	oled_goto_line(line);
	for(byte i=0;i<128;i++)
		oled_put_byte(0);
}

void oled_clear_display()
{
	for(byte i=0;i<8;i++)
	oled_clear_line(i);
	oled_goto_home();
}

void oled_clear_indents()
{
	for(byte i=0;i<8;i++)
	{
		oled_goto_line(i);
		oled_putc(' ');
	}
}

/*
char flipbits(char input){
char temp = 0;

if (input&0x01) temp |= 0x80;
if (input&0x02) temp |= 0x40;
if (input&0x04) temp |= 0x20;
if (input&0x08) temp |= 0x10;
if (input&0x10) temp |= 0x08;
if (input&0x20) temp |= 0x04;
if (input&0x40) temp |= 0x02;
if (input&0x80) temp |= 0x01;

return temp;
}
*/