/*
 * uart.c
 *
 * Created: 02.09.2014 14:07:12
 *  Author: diaaj
 */ 

#include "uart.h"
//#include "defines.h"


void uart_init(unsigned int ubrr)
{
	/*	Set baud rate	*/
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/*	Enable receiver and transmitter	*/
	//UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<TXCIE0)|(1<<RXCIE0);
	/*	Enable receiver and transmitter with RX interrupt */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
	/*	Set frame format: 8data, 2stop bit	*/
	UCSR0C = (1<<URSEL0)|(3<<UCSZ00);
	
	//Call fdevopen. This is what binds printf to put_uart_char. The first
	//parameter is the address of our function that will output a single
	//character, the second parameter is an optional parameter that is
	//used for get functions, ie. receiving a character from the UART.
	//This is the function that uses malloc.

	fdevopen(&uart_transmit, NULL);
}

int uart_transmit( char data, FILE *stream)
{
	/* Wait for empty transmit buffer */
	//while (UCSR0A & (1<<TXC0));
	if (data == '\n')
		uart_transmit('\r', stream);
		
	while (!(UCSR0A & (1<<UDRE0)));
	
	/* Put data into buffer, sends the data */
	UDR0 = data;
	return 0;
}

unsigned char uart_receive( void )
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) );
	/* Get and return received data from buffer */
	return UDR0;
}



