/*
 * oled.h
 *
 * Created: 16.09.2014 15:33:32
 *  Author: diaaj
 */ 


#ifndef OLED_H_
#define OLED_H_

#include <stdio.h>
#include <avr/pgmspace.h>

void oled_init();
void oled_test();

void oled_goto_home();
void oled_goto_line(char line);
//void oled_clear_line(char line);
void oled_goto_pos(char row,char column);

void oled_send_cmd(char* buffer, char buffer_size);
void oled_goto_line(char line);
void oled_put_byte(unsigned char b);
void oled_putc(unsigned char string);
void oled_puts(char * string);
void oled_puts_const(const char * string);
void oled_clear_line(byte line);
void oled_clear_display();
int oled_putc_printf(char  string, FILE *stream);
void oled_clear_indents();
void oled_dualbuf_put_byte(unsigned char b);
void oled_dualbuf_goto_pos(char row, char column, char mod);
#endif /* OLED_H_ */