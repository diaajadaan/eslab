/*
 * adc.c
 *
 * Created: 09.09.2014 12:33:10
 *  Author: diaaj
 */ 

#include <avr/io.h>
#include "defines.h"
#include "adc.h"
#include <avr/delay.h>

volatile bool adc_ready=false;

void adc_init()
{
	clear_bit(DDRD,INTR);
}

byte adc_read(byte channel)
{
	ext_ram = (char *) 0x1400 ; // Start address for the ADC	
	ext_ram[0]= 0x03+channel;
	for(;adc_ready==0;);
	
	adc_ready=false;
	
	return ext_ram[0];
}

void adc_done()
{
	adc_ready=true;
}