/*
 * uart.h
 *
 * Created: 02.09.2014 14:06:26
 *  Author: diaaj
 */ 


#ifndef UART_H_
#define UART_H_
#include <avr/io.h>
#include <stdio.h>

void uart_init(unsigned int ubrr );


int uart_transmit( char data, FILE *stream);


unsigned char uart_receive( void );



#endif /* UART_H_ */