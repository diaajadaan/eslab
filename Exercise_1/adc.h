/*
 * adc.h
 *
 * Created: 09.09.2014 12:33:00
 *  Author: diaaj
 */ 


#ifndef ADC_H_
#define ADC_H_

#include "joy.h"

void adc_init();
byte adc_read(byte channel);

// Used by ADC interrupt to signify that conversion is done
void adc_done();

#endif /* ADC_H_ */