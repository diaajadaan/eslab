/*
 * defines.h
 *
 * Created: 02.09.2014 14:08:24
 *  Author: diaaj
 */ 

#ifndef DEFINES_H_
#define DEFINES_H_

#define F_CPU 4915200
#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1
#define LIFE_PERIOD 100

#define set_bit( reg, bit ) (reg |= (1 << bit))
#define clear_bit( reg, bit ) (reg &= ~(1 << bit))
#define test_bit( reg, bit ) (reg & (1 << bit))

#ifndef loop_until_bit_is_set
	#define loop_until_bit_is_set( reg, bit ) while(!test_bit( reg, bit ) )
#endif

#ifndef loop_until_bit_is_clear
	#define loop_until_bit_is_clear( reg, bit ) while( test_bit( reg, bit ) )
#endif

#define FONT5W
#define CHARACTER_WIDTH 5

#define bits2byte(b7,b6,b5,b4,b3,b2,b1,b0) ((char)((b7<<7)|(b6<<6)|(b5<<5)|(b4<<4)|(b3<<3)|(b2<<2)|(b1<<1)|(b0<<0)))

#define INTR 2
#define INTRPORT PORTD
#define JOYBUTTON 4
#define JOYBUTTONPIN PIND
#define JOYBUTTONPORT PORTD

#define TOUCHR 0
#define TOUCHRPIN PINB
#define LED 2
#define LEDPORT PORTB
#define TOUCHL 3
#define TOUCHLPIN PINB

typedef unsigned char byte;
typedef char bool;
#define true 1;
#define false 0;

volatile char *ext_ram; // Start address for the SRAM
volatile byte current_line;
volatile byte current_menu;
volatile byte current_submenu;
#endif /* DEFINES_H_ */