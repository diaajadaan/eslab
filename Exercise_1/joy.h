/*
 * joy.h
 *
 * Created: 09.09.2014 14:56:05
 *  Author: diaaj
 */ 


#ifndef JOY_H_
#define JOY_H_


#include "adc.h"

typedef struct {
	signed char X;
	signed char Y;
	} joy_pos_t;

typedef struct {
	char Right;
	char Left;
} joy_slide_t;

typedef enum {
	LEFT,
	RIGHT,
	UP,
	DOWN,
	CENTER
	} joy_dir_t;
	

void joy_init();

joy_pos_t joy_getPosition();
joy_slide_t joy_getSliders();
void joy_calibrate();

//returns 'true' if button is pressed
bool joy_button(int button);

joy_dir_t joy_getDirection();


#endif /* JOY_H_ */